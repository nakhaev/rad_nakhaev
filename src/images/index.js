
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

// local dependencies
import upload from './upload.svg';
import defImg from './def-image.svg';
import settingsGif from './settings.gif';
import logoImage from './logo-32x32.png';
import spinnerRed from './spinner-red.svg';
import spinnerBlack from './spinner-black.svg';
import spinnerWhite from './spinner-white.svg';
import defaultAvatar from './default_avatar.svg';

export class DefImage extends PureComponent {
    src = () => this.props.src || this.props.defaultSrc;

    alt = () => this.props.alt || this.props.defaultAlt;

    title = () => this.props.title || this.props.defaultTitle;

    style = () => Object.assign({}, this.props.defaultStyle, this.props.style);

    render () {
        return <img src={this.src()} alt={this.alt()} title={this.title()} style={this.style()} />;
    }
}
// Check
DefImage.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    title: PropTypes.string,
    style: PropTypes.object,
    defaultSrc: PropTypes.string,
    defaultAlt: PropTypes.string,
    defaultTitle: PropTypes.string,
    defaultStyle: PropTypes.object,
};
// Def
DefImage.defaultProps = {
    src: null,
    alt: null,
    title: null,
    style: {},
    defaultSrc: defImg,
    defaultAlt: 'image',
    defaultTitle: 'image',
    defaultStyle: {},
};

export const Logo = props => (
    <DefImage
        defaultSrc={logoImage}
        defaultAlt="DFusion Tech"
        defaultTitle="DFusion Tech"
        {...props}
    />
);

export const Avatar = props => (
    <DefImage
        defaultAlt="User"
        defaultTitle="User"
        defaultSrc={defaultAvatar}
        defaultStyle={{ borderRadius: '50%' }}
        {...props}
    />
);

export const CloudImage = props => (
    <DefImage
        defaultAlt="Upload to cloud"
        defaultTitle="Upload to cloud"
        defaultSrc={upload}
        {...props}
    />
);

export const SettingGif = props => (
    <DefImage
        defaultAlt="Settings"
        defaultTitle="Settings"
        defaultSrc={settingsGif}
        {...props}
    />
);

export const SpinnerWhite = props => (
    <DefImage
        defaultAlt="Loading"
        defaultTitle="Loading"
        defaultSrc={spinnerWhite}
        {...props}
    />
);

export const SpinnerBlack = props => (
    <DefImage
        defaultAlt="Loading"
        defaultTitle="Loading"
        defaultSrc={spinnerBlack}
        {...props}
    />
);

export const SpinnerRed = props => (
    <DefImage
        defaultAlt="Loading"
        defaultTitle="Loading"
        defaultSrc={spinnerRed}
        {...props}
    />
);

/**
 * Wrapped spinners
 */
export function Spinner ({ black, white, ...attr }) {
    if (white) { return <SpinnerWhite {...attr} />; }
    if (black) { return <SpinnerBlack {...attr} />; }
    return <SpinnerRed {...attr} />;
}
// Check
Spinner.propTypes = {
    black: PropTypes.bool,
    white: PropTypes.bool,
};
// Def
Spinner.defaultProps = {
    black: false,
    white: false,
};
