
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { Dropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

// local dependencies
import { Avatar } from '../images/index';
import * as ROUTES from '../constants/routes';
import { filters } from '../components/filter';
import { APP, PRIVATE } from '../actions/types';

class UserMenu extends PureComponent {
    handleSignOut = () => this.props.signOut();

    handleToggleMenu = () => this.props.toggleMenu(!this.props.expanded);

    userRoles () {
        const { user: { roles = [] } } = this.props;
        return roles.map(role => filters.humanize(role.name)).join();
    }

    render () {
        const { user } = this.props;
        return (<Dropdown id="user-action-dropdown" className="pull-right">
            <button className="btn btn-user-avatar no-events" disabled={true}>
                <strong> { user.fullName } </strong>&nbsp;
                <Avatar src={ user.avatar } alt={ user.fullName } style={{ width: '35px', height: '35px' }} />
            </button>
            <Dropdown.Toggle bsSize="lg"/>
            <Dropdown.Menu>
                <MenuItem disabled={true}> { this.userRoles() } </MenuItem>
                <LinkContainer exact to={ROUTES.USERS.LINK_EDIT({ id: user.id })}>
                    <MenuItem eventKey="1">
                        <i className="fa fa-user" aria-hidden="true"> </i>
                        &nbsp;Profile
                    </MenuItem>
                </LinkContainer>
                <MenuItem eventKey="2" onClick={this.handleToggleMenu}> Toggle menu </MenuItem>
                <MenuItem divider={true} />
                <MenuItem eventKey="3" onClick={this.handleSignOut}>
                    <i className="fa fa-sign-out" aria-hidden="true"> </i>
                    &nbsp;Sign Out
                </MenuItem>
            </Dropdown.Menu>
        </Dropdown>);
    }
}
// Check
UserMenu.propTypes = {
    expanded: PropTypes.bool,
    user: PropTypes.object.isRequired,
    signOut: PropTypes.func.isRequired,
    toggleMenu: PropTypes.func.isRequired,
};
// Def
UserMenu.defaultProps = {
    expanded: false,
};
// Export
export default connect(
    state => ({
        user: state.app.user,
        expanded: state.privateLayout.expanded
    }),
    dispatch => ({
        signOut: () => dispatch({ type: APP.SIGN_OUT.REQUEST }),
        toggleMenu: expanded => dispatch({ type: PRIVATE.TOGGLE_ASIDE, expanded }),
    })
)(UserMenu);
