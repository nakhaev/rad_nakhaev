
// outsource dependencies

// local dependencies
import { PRIVATE } from '../actions/types';

import welcome from './welcome/reducer';
import users from './users/reducers';

export default {
    privateLayout,
    welcome,
    users,
};

function privateLayout (state = { expanded: true }, action) {
    const { type, expanded } = action;
    switch (type) {
        default:
            break;
        case PRIVATE.TOGGLE_ASIDE:
            state = { ...state, expanded };
            break;
    }
    // /@private/.test(type)&&
    // console.log(`%c REDUCER privateLayout ${type} `, 'color: #fff; background: #232323; font-size: 18px;'
    //     ,'\n state:', state
    //     ,'\n action:', action
    //     // ,'\n arguments:', arguments
    // );
    return state;
}
