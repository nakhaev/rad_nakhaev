
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';

// local dependencies
import { Logo } from '../../images';
import Input from '../../components/input';
import { PUBLIC } from '../../actions/types';
import Preloader from '../../components/preloader';
import CenteredBox from '../../components/centered-box';
import ErrorMessage from '../../components/alert-error';
import { SIGN_IN, FORGOT_PASSWORD } from '../../constants/routes';


class ChangePassword extends PureComponent {
    constructor (...args) {
        super(...args);
        this.state = {};
    }

    componentDidMount () {
        const { validateToken, match: { params: { token } } } = this.props;
        validateToken({ token });
    }

    render () {
        const {
            invalid, invalidToken, expectAnswer,
            handleSubmit, changePassword,
            errorMessage, clearError
        } = this.props;
        return (<CenteredBox style={{ width: '460px' }}>
            <div className="row offset-bottom-8">
                <div className="col-xs-6 col-xs-offset-3">
                    <Logo className="img-responsive" />
                </div>
            </div>
            <div className="row offset-bottom-8">
                <h1 className="col-xs-12 text-center text-drama">
                    <strong> Change password </strong>
                </h1>
            </div>
            <div className="row offset-bottom-8">
                <div className="col-xs-12">
                    <Preloader type="BOX" active={expectAnswer}>
                        { invalidToken ? (
                            <h4 className="col-xs-12 text-justify">
                                Whoa there! The request token for this page is invalid.
                                It may have already been used, or expired because it is too old.
                                Please go back to the
                                <Link to={FORGOT_PASSWORD.LINK()}> forgot password page </Link>
                                and try again.
                                <br/>
                                <small> it was probably just a mistake </small>
                            </h4>
                        ) : (
                            <div className="col-xs-12">
                                <form name="changePassword" onSubmit={ handleSubmit(changePassword) }>
                                    <div className="offset-bottom-8">
                                        <Field
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                            component={ Input }
                                            disabled={ expectAnswer }
                                            className="form-control input-lg"
                                            label={ <span className="h3 text-black"> Password </span> }
                                        />
                                    </div>
                                    <div className="offset-bottom-8">
                                        <Field
                                            type="password"
                                            component={ Input }
                                            name="confirmPassword"
                                            placeholder="Confirm"
                                            disabled={ expectAnswer }
                                            className="form-control input-lg"
                                            label={ <span className="h3 text-black"> Confirm password </span> }
                                        />
                                    </div>
                                    <button
                                        type="submit"
                                        disabled={ invalid || expectAnswer }
                                        className="btn btn-lg btn-block btn-primary offset-bottom-6"
                                    >
                                        <span> Change password </span>
                                        { expectAnswer&&(<i className="fa fa-spinner fa-spin fa-fw"> </i>) }
                                    </button>
                                    <ErrorMessage active title={'Error:'} message={errorMessage} onChange={clearError}/>
                                </form>
                            </div>
                        )}
                    </Preloader>
                </div>
            </div>
            <div className="row offset-bottom-10">
                <div className="col-xs-6"> {/* Place 1*/} </div>
                <div className="col-xs-6 text-right">
                    <Link to={SIGN_IN.LINK()}> Sign In </Link>
                </div>
            </div>
        </CenteredBox>);
    }
}
// Check
ChangePassword.propTypes = {
    invalid: PropTypes.bool,
    invalidToken: PropTypes.bool,
    expectAnswer: PropTypes.bool,
    errorMessage: PropTypes.string,
    match: PropTypes.object.isRequired,
    clearError: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    validateToken: PropTypes.func.isRequired,
    changePassword: PropTypes.func.isRequired,
};
// Def
ChangePassword.defaultProps = {
    invalid: false,
    invalidToken: true,
    expectAnswer: false,
    errorMessage: null,
};

export default reduxForm({
    form: 'changePassword',
    /**
     * @param { Object } values - named properties of input data
     * @param { Object } meta - information about form status
     * @returns { Object } - named errors
     * @function validate
     * @public
     */
    validate: (values, meta) => {
        const errors = {};
        // PASSWORD
        if (!values.password) {
            errors.password = 'Password is required';
        } else if (values.password.length < 8) {
            errors.password = 'Password must contain at least 8 symbol character';
        }
        // CONFIRM PASSWORD
        if (!values.confirmPassword) {
            errors.confirmPassword = 'Password confirmation is required';
        } else if (values.confirmPassword !== values.password) {
            errors.confirmPassword = 'Passwords confirmation do not match with password';
        }
        return errors;
    },
})(connect(
    // mapStateToProps
    state => ({ ...state.changePassword }),
    // mapDispatchToProps
    dispatch => ({
        changePassword: ({ password }) => dispatch({ type: PUBLIC.CHANGE_PASSWORD.REQUEST, password }),
        validateToken: ({ token }) => dispatch({ type: PUBLIC.VALIDATE_PASSWORD_TOKEN.REQUEST, token }),
        clearError: () => dispatch({ type: PUBLIC.CHANGE_PASSWORD.CLEAR }),
    })
)(ChangePassword));
