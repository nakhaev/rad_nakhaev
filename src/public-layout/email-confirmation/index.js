
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { PureComponent } from 'react';

// local dependencies
import { Logo } from '../../images';
import { PUBLIC } from '../../actions/types';
import { SIGN_IN } from '../../constants/routes';
import Preloader from '../../components/preloader';
import CenteredBox from '../../components/centered-box';


class EmailConfirmation extends PureComponent {
    constructor (...args) {
        super(...args);
        this.state = {};
    }

    componentDidMount () {
        const { validateToken, match: { params: { token } } } = this.props;
        validateToken({ token });
    }

    render () {
        const { expectAnswer, invalidToken } = this.props;
        return (<CenteredBox style={{ width: '460px' }}>
            <div className="row offset-bottom-8">
                <div className="col-xs-6 col-xs-offset-3">
                    <Logo className="img-responsive" />
                </div>
            </div>
            <div className="row offset-bottom-0">
                <h1 className="col-xs-12 text-center text-drama">
                    <strong> Email Confirmation </strong>
                </h1>
            </div>
            <div className="row offset-bottom-8">
                <div className="col-xs-12">
                    <Preloader type="BOX" active={expectAnswer}>
                        { invalidToken ? (
                            <h3 className="col-xs-12 text-justify">
                                Whoa there! The request token for this page is invalid.
                                It may have already been used, or expired because it is too old.
                                Please go back to the and try again.
                            </h3>
                        ) : (
                            <h3 className="col-xs-12 text-justify">
                                The email address was successfully verified. Welcome aboard !
                            </h3>
                        )}
                    </Preloader>
                </div>
            </div>
            <div className="row offset-bottom-10">
                <div className="col-xs-6"> {/* Place 1*/} </div>
                <div className="col-xs-6 text-right">
                    <Link to={SIGN_IN.LINK()}> Sign In </Link>
                </div>
            </div>
        </CenteredBox>);
    }
}
// Check
EmailConfirmation.propTypes = {
    expectAnswer: PropTypes.bool,
    invalidToken: PropTypes.bool,
    match: PropTypes.object.isRequired,
    validateToken: PropTypes.func.isRequired,
};
// Def
EmailConfirmation.defaultProps = {
    invalidToken: true,
    expectAnswer: false,
};

export default connect(
    // mapStateToProps
    state => ({ ...state.emailConfirmation }),
    // mapDispatchToProps
    dispatch => ({
        validateToken: ({ token }) => dispatch({ type: PUBLIC.EMAIL_CONFIRMATION.REQUEST, token }),
    })
)(EmailConfirmation);
