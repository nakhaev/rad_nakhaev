
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';

// local dependencies
import Input from '../../components/input';
import { PUBLIC } from '../../actions/types';
import ErrorMessage from '../../components/alert-error';

class SignUpForm extends PureComponent {
    render () {
        const { invalid, handleSubmit, expectAnswer, signUp, errorMessage, clearError } = this.props;
        return (<form name="signUpForm" onSubmit={ handleSubmit(signUp) }>
            <div className="offset-bottom-4">
                <Field
                    type="text"
                    name="firstName"
                    placeholder="Nick / Name"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black required-asterisk"> First name </span> }
                />
            </div>
            <div className="offset-bottom-4">
                <Field
                    type="text"
                    name="lastName"
                    placeholder="Last name"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black"> Last name </span> }
                />
            </div>
            <div className="offset-bottom-4">
                <Field
                    type="mail"
                    name="email"
                    placeholder="Email"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black required-asterisk"> Email </span> }
                />
            </div>
            <div className="offset-bottom-4">
                <Field
                    required
                    name="password"
                    type="password"
                    placeholder="Password"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black required-asterisk"> Password </span> }
                />
            </div>
            <div className="offset-bottom-6">
                <Field
                    type="password"
                    component={ Input }
                    name="confirmPassword"
                    placeholder="Confirm"
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black required-asterisk"> Confirm password </span> }
                />
            </div>
            <button
                type="submit"
                disabled={ invalid || expectAnswer }
                className="btn btn-lg btn-block btn-primary"
            >
                <span> Login </span>
                { expectAnswer&&(<i className="fa fa-spinner fa-spin fa-fw"> </i>) }
            </button>
            <ErrorMessage active title={'Error:'} message={errorMessage} onChange={clearError}/>
        </form>);
    }
}
// Check
SignUpForm.propTypes = {
    invalid: PropTypes.bool,
    expectAnswer: PropTypes.bool,
    errorMessage: PropTypes.string,
    signUp: PropTypes.func.isRequired,
    clearError: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};
// Def
SignUpForm.defaultProps = {
    invalid: false,
    expectAnswer: false,
    errorMessage: null,
};

export default reduxForm({
    form: 'signUpForm',
    /**
     * @param { Object } values - named properties of input data
     * @param { Object } meta - information about form status
     * @returns { Object } - named errors
     * @function validate
     * @public
     */
    validate: (values, meta) => {
        const errors = {};
        // FIRST
        if (!values.firstName) {
            errors.firstName = 'First name is required';
        } else if (values.firstName.length < 3) {
            errors.firstName = 'First name must contain at least 3 symbol character';
        }
        // LAST
        // NOTE no validation

        // EMAIL
        if (!values.email) {
            errors.email = 'Email is required';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Invalid email address';
        }
        // PASSWORD
        if (!values.password) {
            errors.password = 'Password is required';
        } else if (values.password.length < 8) {
            errors.password = 'Password must contain at least 8 symbol character';
        }
        // CONFIRM PASSWORD
        if (!values.confirmPassword) {
            errors.confirmPassword = 'Password confirmation is required';
        } else if (values.confirmPassword !== values.password) {
            errors.confirmPassword = 'Passwords confirmation do not match with password';
        }
        return errors;
    },
})(connect(
    // mapStateToProps
    state => ({ ...state.signUp }),
    // mapDispatchToProps
    dispatch => ({
        signUp: formData => dispatch({ type: PUBLIC.SIGN_UP.REQUEST, ...formData }),
        clearError: () => dispatch({ type: PUBLIC.SIGN_UP.CLEAR }),
    })
)(SignUpForm));
