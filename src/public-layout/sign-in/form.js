
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';

// local dependencies
import { APP } from '../../actions/types';
import Input from '../../components/input';
import ErrorMessage from '../../components/alert-error';

class SignInForm extends PureComponent {
    render () {
        const { invalid, handleSubmit, expectAnswer, signIn, errorMessage, clearError } = this.props;
        return (<form name="signInForm" onSubmit={ handleSubmit(signIn) }>
            <div className="offset-bottom-4">
                <Field
                    type="mail"
                    name="email"
                    placeholder="Email"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black"> Email </span> }
                />
            </div>
            <div className="offset-bottom-6">
                <Field
                    required
                    name="password"
                    type="password"
                    placeholder="Password"
                    component={ Input }
                    disabled={ expectAnswer }
                    className="form-control input-lg"
                    label={ <span className="h3 text-black"> Password </span> }
                />
            </div>
            <button
                type="submit"
                disabled={ invalid || expectAnswer }
                className="btn btn-lg btn-block btn-primary offset-bottom-6"
            >
                <span> Login </span>
                { expectAnswer&&(<i className="fa fa-spinner fa-spin fa-fw"> </i>) }
            </button>
            <ErrorMessage active title={'Error:'} message={errorMessage} onChange={clearError}/>
        </form>);
    }
}
// Check
SignInForm.propTypes = {
    invalid: PropTypes.bool,
    expectAnswer: PropTypes.bool,
    errorMessage: PropTypes.string,
    signIn: PropTypes.func.isRequired,
    clearError: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};
// Def
SignInForm.defaultProps = {
    invalid: false,
    expectAnswer: false,
    errorMessage: null,
};

export default reduxForm({
    form: 'signInForm',
    /**
     * @param { Object } values - named properties of input data
     * @param { Object } meta - information about form status
     * @returns { Object } - named errors
     * @function validate
     * @public
     */
    validate: (values, meta) => {
        const errors = {};
        // EMAIL
        if (!values.email) {
            errors.email = 'Email is required';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Invalid email address';
        }
        // PASSWORD
        if (!values.password) {
            errors.password = 'Password is required';
        } else if (values.password.length < 8) {
            errors.password = 'Password must contain at least 8 symbol character';
        }
        return errors;
    },
})(connect(
    // mapStateToProps
    state => ({ ...state.signIn }),
    // mapDispatchToProps
    dispatch => ({
        signIn: ({ email, password }) => dispatch({ type: APP.SIGN_IN.REQUEST, email, password }),
        clearError: () => dispatch({ type: APP.SIGN_IN.CLEAR }),
    })
)(SignInForm));
