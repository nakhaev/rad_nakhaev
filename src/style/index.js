
// import with fonts ...
// TODO: try to make override for variables
import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/css/bootstrap-theme.css';
// font awesome
import 'font-awesome/css/font-awesome.css';
// toastr
import './vendors/toastr.css';
// react-select
import 'react-select/dist/react-select.css';
// react-image-crop
import 'react-image-crop/dist/ReactCrop.css';
// react-draft-wysiwyg
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// react-datetime
import 'react-datetime/css/react-datetime.css';
// app styles (result of preprocessor less)
import './all.css';
// TODO: try to connect something else ...
