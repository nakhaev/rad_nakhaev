
# React application dummy **(RAD)**

This example was created to accelerate the start of the project using "React.js". Project based on **react-create-app**
( [GITHUB](https://github.com/facebookincubator/create-react-app) )
( [GUIDE](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md) )
( [README.md](https://bitbucket.org/SPerekhrest/rad/src/f0178152a8dec75eeb72253fb8ed8407f84e6bbd/create-react-app.md?at=master&fileviewer=file-view-default) )

## connected packages
- [React](https://reactjs.org/docs/hello-world.html)
- [Axios](https://www.npmjs.com/package/axios)
- [Lodash](https://lodash.com/docs/4.17.4)
- [Moment](https://momentjs.com/)
- [bootstrap](https://getbootstrap.com/docs/3.3)
- [react-bootstrap](https://react-bootstrap.github.io)
- [react-redux](https://github.com/reactjs/react-redux)
- [react-router](https://reacttraining.com/react-router/web/example/basic)
- [redux-form](https://redux-form.com/7.2.0/)
- [redux-thunk](https://github.com/gaearon/redux-thunk)
- [react-redux-toastr](https://www.npmjs.com/package/react-redux-toastr)
- [redux-saga](https://redux-saga.js.org)


### Introduction

Whenever there is a need to write a new web interface using React.js, we are faced with the problem of a lack of basic abstractions. Possessing a powerful template engine, React absolutely does not solve the issues of code organization. In this connection we are forced to solve these problems every time. We are helped to solve them by Babel (ES6) preprocessor. But this is not enough when there is a need to organize the structure of the project. In this application billet, a minimal set of proven ready-made solutions for the development of medium and large applications is collected.


#### Fork and run

Fork this repo.

```
> git clone https://SPerekhrest@bitbucket.org/SPerekhrest/rad.git
```
install dependencies.
```
> npm install
```
Then start the development process.
```
> npm run start
```
To run preprocessor less
```
> gulp less:watch
```

#### Build

Without environment configuration used the **develpment** config.

```
> npm install
> npm run build
```

#### Setup custom configuration

Flow of **react-create-app** [GUIDE](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md) fully supported. To setup client side configuration simplify flow using non standard ```env``` **REACT_APP_ENV**.

#### To setup configuration without depending on build type.

##### example PRODUCTION
Defined as a production and setup in package.js
Source of configuration file in ./src/constant/production.js

```
> npm run build-prod
// the same as
> set REACT_APP_ENV=production && npm run build
```

Run locally
```
> set REACT_APP_ENV=production && npm run start
```

##### example STAGING
Source of configuration file in ./src/constant/staging.js 

```
> set REACT_APP_ENV=staging && npm run build
```

Run locally
```
> set REACT_APP_ENV=staging && npm run start
```

##### example DEVELOPMENT
Default value for configuration is development. 
Source of configuration file in ./src/constant/development.js 

```
> npm run build
// the same as
> set REACT_APP_ENV=development && npm run build
```

Run locally
```
> npm run start
```
